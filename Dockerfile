FROM amd64/debian

RUN apt-get update
RUN apt-get install -y g++


RUN mkdir -p /var/tmp/bubblesort
COPY . /var/tmp/bubblesort
ADD cicd/deploy.sh /var/tmp/bubblesort/deploy.sh
WORKDIR  /var/tmp/bubblesort

RUN /bin/sh deploy.sh
#ENTRYPOINT ["/bin/sh", "/deploy.sh"]

#ENTRYPOINT ["bubblesort"]
#RUN chmod +x deploy.sh
#RUN ./deploy.sh
RUN bubblesort
#ENTRYPOINT ["chmod", "+x", "./deploy.sh"]
#ENTRYPOINT ["./deploy.sh"]
#CMD ["bubblesort"]