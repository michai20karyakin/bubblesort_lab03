#include <gtest/gtest.h>

#include "../include/bubblesort.hpp"

TEST(BubbleSort, SortEmptyVector)
{
    std::vector<int> vec;

    BubbleSort a(vec);
    a.sort();

    auto sortVec = a.getArray();
    ASSERT_EQ(vec.size(), sortVec.size()) << "Sort vector size not equal source vector";
}

TEST(BubbleSort, SortVector)
{
    std::vector<int> mustVec{-21, -4, -3, 0, 0, 1, 2, 3, 3, 3, 4, 32};
    std::vector<int> sourceVec{32, 3, -4, 0, 3, 2, 1, -21, -3, 0, 3, 4};

    BubbleSort a(sourceVec);
    a.sort();


    auto sortVec = a.getArray();
    ASSERT_EQ(mustVec.size(), sortVec.size()) << "Sort vector size not equal source vector";
    
    for(std::vector<int>::size_type i = 0; i < sortVec.size(); ++i) 
    {
        EXPECT_EQ(sortVec[i], mustVec[i]) << "Vectors x and y differ at index " << i;
    }
}
