#ifndef BUBBLESORT_HPP
#define BUBBLESORT_HPP

#include <vector>

class BubbleSort
{
private:

    std::vector<int> array;

public:

    BubbleSort();
    BubbleSort(std::vector<int> array);

    void sort();
    void print();
    void read();
    
    std::vector<int> getArray();

};

#endif