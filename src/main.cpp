#include <iostream>

#include "include/bubblesort.hpp"

int main() 
{
    BubbleSort a;
    std::cout << "Введите массив целых чисел:" << std::endl;
    a.read();    
    a.sort();
    std::cout << "Отсортированный массив:" << std::endl;
    a.print();

    return 0;
}