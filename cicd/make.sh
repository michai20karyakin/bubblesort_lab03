#!/bin/bash 
set -e

pwd
ls -al
make -C src
rm -rf usr
mkdir -p usr/bin
cp src/bubblesort usr/bin/